#ifndef TST_T05_H
#define TST_T05_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "zamien.h"

using namespace testing;

class Testclass : public testing::Test
{
public:
    virtual void SetUp(){}
    Zamien nazwa;
};

class Testclass_b : public Testclass,
        public testing::WithParamInterface<string>
{
    virtual void SetUp(){
        Zamien nazwa;
    }
};

TEST_F(Testclass, t05)
{
    string in="abc";
    string ret = nazwa.zamien_a(in);
    EXPECT_EQ(ret, "^bc");
}

TEST_F(Testclass, t06)
{
    string in="abc";
    string ret = nazwa.zamien_a(in);
    EXPECT_NE(ret, "%bc");
}

TEST_P(Testclass_b, t07)
{
    string ret = nazwa.zamien_a(GetParam());
    EXPECT_NE(ret, "abc");
}

#endif // TST_T05_H
